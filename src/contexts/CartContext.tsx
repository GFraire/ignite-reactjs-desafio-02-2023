import { ReactNode, createContext, useEffect, useState } from "react";
import { CoffeItemObject } from "@/pages/Home/components/CoffeItem";

interface CartContextType {
  cartItems: CartItemsType[];
  totalOfItems: number;
  address: AddressProps;
  isAddressValidated: boolean;
  onSetCartItems: (cartItem: CartItemsType) => void;
  onRemoveCartItem: (cartItem: CartItemsType) => void;
  onSetAddress: (address: AddressProps) => void;
}

interface CartItemsType extends CoffeItemObject {
  counter: number;
}

interface CartContextProviderProps {
  children: ReactNode;
}

interface AddressProps {
  CEP: string;
  rua: string;
  numero: string;
  complemento?: string;
  bairro: string;
  UF: string;
}

export const CartContext = createContext({} as CartContextType);

export function CartContextProvider({ children }: CartContextProviderProps) {
  const [cartItems, setCartItems] = useState<CartItemsType[]>([]);
  const [totalOfItems, setTotalOfItems] = useState(0);
  const [address, setAdress] = useState({} as AddressProps);
  const [isAddressValidated, setIsAddressValidated] = useState(false);

  useEffect(() => {
    let summedPrices = 0;

    cartItems.forEach((cartItem) => {
      summedPrices += cartItem.price * cartItem.counter;
    });

    setTotalOfItems(summedPrices);
  }, [cartItems]);

  useEffect(() => {
    if (
      address.CEP &&
      address.UF &&
      address.bairro &&
      address.numero &&
      address.rua
    )
      setIsAddressValidated(true);
    else setIsAddressValidated(false);
  }, [address]);

  function onSetCartItems(cartItem: CartItemsType) {
    const itemExists = cartItems.some((item) => item.title === cartItem.title);

    if (!itemExists) {
      setCartItems([...cartItems, cartItem]);
    } else {
      if (cartItem.counter === 0) {
        onRemoveCartItem(cartItem);
      } else {
        const updatedCartItems = cartItems.map((item) => {
          if (item.title === cartItem.title) {
            item.counter = cartItem.counter;
          }

          return item;
        });

        setCartItems(updatedCartItems);
      }
    }
  }

  function onRemoveCartItem(cartItem: CartItemsType) {
    const filteredCartItems = cartItems.filter((item) => {
      return item.title !== cartItem.title;
    });

    setCartItems(filteredCartItems);
  }

  function onSetAddress(address: AddressProps) {
    setAdress(address);
  }

  return (
    <CartContext.Provider
      value={{
        totalOfItems,
        cartItems,
        address,
        isAddressValidated,
        onSetCartItems,
        onRemoveCartItem,
        onSetAddress,
      }}
    >
      {children}
    </CartContext.Provider>
  );
}
