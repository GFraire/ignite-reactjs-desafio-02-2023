import styled from "styled-components";
import { DefaultThemeColorsType } from "../../@types/styled";

interface IconButtonContainerProps {
  variant: DefaultThemeColorsType
}

export const IconButtonContainer = styled.div<IconButtonContainerProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;
  padding: 0.5rem;
  border-radius: 6px;
  background-color: ${(props) => props.theme[props.variant]};

  cursor: pointer;
`;
