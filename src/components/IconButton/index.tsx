import { ReactNode } from "react";
import { IconButtonContainer } from "./styles";
import { DefaultThemeColorsType } from "@/@types/styled";

interface IconButtonProps {
  children: ReactNode;
  variant: DefaultThemeColorsType
}

export function IconButton({ children, variant }: IconButtonProps) {
  return <IconButtonContainer variant={variant}>{children}</IconButtonContainer>;
}
