import {
  CounterCartContainer,
  HeaderContainer,
  LocalizationContainer,
} from "./styles";
import { MapPin, ShoppingCart } from "phosphor-react";
import { useTheme } from "styled-components";
import { NavLink } from "react-router-dom";
import { useContext } from "react";
import { IconButton } from "@/components/IconButton";
import { CartContext } from "@/contexts/CartContext";
import logo from "@/assets/logo.svg";

export function Header() {
  const { purple, "yellow-dark": yellowDark } = useTheme();
  const { cartItems } = useContext(CartContext);

  const counterCartItems = cartItems.length;

  return (
    <HeaderContainer>
      <NavLink to="/" title="Cafés">
        <img src={logo} />
      </NavLink>

      <div>
        <LocalizationContainer>
          <MapPin weight="fill" color={purple} size={22} />

          <span>Porto Alegre, RS</span>
        </LocalizationContainer>

        <NavLink to="/cart" title="Carrinho">
          <IconButton variant="yellow-light">
            <ShoppingCart weight="fill" color={yellowDark} size={22} />

            {counterCartItems > 0 && (
              <CounterCartContainer>{counterCartItems}</CounterCartContainer>
            )}
          </IconButton>
        </NavLink>
      </div>
    </HeaderContainer>
  );
}
