import styled from "styled-components";

export const HeaderContainer = styled.header`
  max-width: 70rem;
  margin: auto;
  padding-top: 2rem;
  padding-bottom: 2rem;

  display: flex;
  justify-content: space-between;

  div {
    display: flex;
    align-items: center;
    gap: 12px;
  }
`;

const BaseContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  padding: .5rem;
  border-radius: 6px;
`

export const LocalizationContainer = styled(BaseContainer)`
  background-color: ${(props) => props.theme["purple-light"]};
  color: ${(props) => props.theme["purple-dark"]};
  font-size: 0.875rem;
`;

export const CounterCartContainer = styled.div`
  position: absolute;
  right: -10px;
  bottom: -5px;

  display: flex;
  justify-content: center;
  align-items: center;

  width: 20px;
  height: 20px;

  background-color: ${props => props.theme["yellow-dark"]};
  color: ${props => props.theme["yellow-light"]};
  border-radius: 100%;
`