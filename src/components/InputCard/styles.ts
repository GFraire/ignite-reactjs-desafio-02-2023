import styled from "styled-components";

export const InputItemContainer = styled.div`
  position: relative;

  input {
    max-width: 4.5rem;
    text-align: center;
    border-radius: 6px;
    padding: 0.5rem 0.25rem;
    border: none;

    background-color: ${(props) => props.theme["base-button"]};
    color: ${(props) => props.theme["base-title"]};
    font-size: 1rem;
    font-weight: 700;
  }
`;

export const AbsoluteButtonContainer = styled.button`
  position: absolute;
  margin: 0;
  border: none;

  cursor: pointer;

  background-color: transparent;

  top: 50%;
  transform: translateY(-50%);

  &:first-child {
    left: 0.5rem;
  }

  &:last-child {
    right: 0.5rem;
  }
`

export const ButtonContainer = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 4px;

  padding: 0px 8px;

  width: 100%;

  border: none;
  cursor: pointer;
  background-color: ${(props) => props.theme["base-button"]};
  color: ${(props) => props.theme["base-text"]};
  font-size: 0.75rem;
`