import { useContext, useEffect, useState } from "react";
import { AbsoluteButtonContainer, ButtonContainer, InputItemContainer } from "./styles";
import { Minus, Plus, Trash } from "phosphor-react";
import { useTheme } from "styled-components";
import { CoffeItemObject } from "../../pages/Home/components/CoffeItem/index";
import { CartContext } from "@/contexts/CartContext";

interface InputCartProps {
  coffeItem: CoffeItemObject;
  variant?: "counter" | "button";
}

export function InputCart({ coffeItem, variant = "counter" }: InputCartProps) {
  const { cartItems, onSetCartItems, onRemoveCartItem } = useContext(CartContext);
  const [counter, setCounter] = useState(() => {
    if (cartItems.length) {
      const item = cartItems.find(
        (cartItem) => cartItem.title === coffeItem.title
      );
      if (item) {
        return item.counter;
      }
    }

    return 0;
  });
  const [isFirstRenderization, setIsFirstRenderization] = useState(true);
  const { purple } = useTheme();

  useEffect(() => {
    const cartItem = {
      ...coffeItem,
      counter,
    };

    if (isFirstRenderization) {
      setIsFirstRenderization(false);
    } else {
      onSetCartItems(cartItem);
    }
  }, [counter]);

  function handleDecreaseItemCart() {
    if (counter > 0) {
      setCounter((state) => {
        return state - 1;
      });
    }
  }

  function handleIncreaseItemCart() {
    if (counter < 99) {
      setCounter((state) => {
        return state + 1;
      });
    }
  }

  function handleRemoveItemCart() {
    const cartItem = {
      ...coffeItem,
      counter,
    };

    onRemoveCartItem(cartItem)
  }

  if (variant === "counter")
    return (
      <InputItemContainer>
        <AbsoluteButtonContainer onClick={handleDecreaseItemCart}>
          <Minus weight="bold" size={14} color={purple} />
        </AbsoluteButtonContainer>

        <input type="text" inputMode="numeric" readOnly value={counter} />

        <AbsoluteButtonContainer onClick={handleIncreaseItemCart}>
          <Plus weight="bold" size={14} color={purple} />
        </AbsoluteButtonContainer>
      </InputItemContainer>
    );
  else
    return (
      <InputItemContainer>
        <ButtonContainer onClick={handleRemoveItemCart}>
          <Trash weight="bold" size={18} color={purple} />
          REMOVER
        </ButtonContainer>
      </InputItemContainer>
    );
}
