import { useState, useEffect } from 'react';

const useImage = (fileName: string) => {
  const [image, setImage] = useState("");

  useEffect(() => {
    // Carregue a imagem com base no nome do arquivo
    import(`../assets/${fileName}.png`)
      .then((imageModule) => setImage(imageModule.default))
      .catch((error) => console.error('Erro ao carregar imagem:', error));
  }, [fileName]);

  return image;
};

export default useImage;

