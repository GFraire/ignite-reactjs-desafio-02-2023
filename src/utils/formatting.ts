export function formatToBRNumber(value: number, decimals = 2) {
  return value.toFixed(decimals).replace(".", ",")
}