import {
  CoffeItemContainer,
  CoffeItemContentContainer,
  CoffeItemHeaderContainer,
  CoffeItemPriceContainer,
} from "./styles";
import { InputCart } from "../../../../components/InputCard";
import { ShoppingCart } from "phosphor-react";
import { useTheme } from "styled-components";
import useImage from "@/hooks/useImage";
import { IconButton } from "@/components/IconButton";

export interface CoffeItemObject {
  title: string;
  description: string;
  price: number;
  tags: string[];
  image: string;
}

interface CoffeItemProps {
  coffeItem: CoffeItemObject;
}

export function CoffeItem({ coffeItem }: CoffeItemProps) {
  const { white } = useTheme();
  const imagePath = useImage(coffeItem.image)
  const priceFormated = coffeItem.price.toFixed(2).replace(".", ",");

  return (
    <CoffeItemContainer>
      <CoffeItemHeaderContainer>
        <img src={imagePath} alt="Copo de café" />

        <div>
          {coffeItem.tags.map((tag) => (
            <span key={tag}>{tag}</span>
          ))}
        </div>
      </CoffeItemHeaderContainer>

      <CoffeItemContentContainer>
        <strong>{coffeItem.title}</strong>

        <span>{coffeItem.description}</span>

        <CoffeItemPriceContainer>
          <span>
            R$ <strong>{priceFormated}</strong>
          </span>

          <div>
            <InputCart coffeItem={coffeItem} />

            <IconButton variant="purple-dark">
              <ShoppingCart weight="fill" color={white} size={22} />
            </IconButton>
          </div>
        </CoffeItemPriceContainer>
      </CoffeItemContentContainer>
    </CoffeItemContainer>
  );
}
