import styled from "styled-components";

export const CoffeItemContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 16rem;

  margin-top: 2rem;
  background-color: ${(props) => props.theme["base-card"]};
  border-radius: 6px 36px 36px 6px;
`;

export const CoffeItemHeaderContainer = styled.div`
  margin-top: -1.25rem;
  margin-bottom: 1rem;

  display: flex;
  align-items: center;
  flex-direction: column;

  img {
    width: 120px;
    margin-bottom: 1rem;
  }

  span {
    padding: 0.25rem 0.5rem;
    border-radius: 100px;
    background-color: ${(props) => props.theme["yellow-light"]};

    font-size: 0.75rem;
    color: ${(props) => props.theme["yellow-dark"]};
    font-weight: 700;

    &:not(:first-child) {
      margin-left: 4px;
    }
  }
`;

export const CoffeItemContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  strong {
    font-family: "Baloo 2", sans-serif;
    font-size: 1.25rem;
    color: ${(props) => props.theme["base-subtitle"]};

    line-height: 130%;
    display: inline-block;
    margin-bottom: 0.5rem;
  }

  span {
    color: ${(props) => props.theme["base-label"]};
    font-size: 0.875rem;
    line-height: 130%;
    text-align: center;
    margin: 0 1.25rem;
  }
`;

export const CoffeItemPriceContainer = styled.div`
  padding: 1rem 1.25rem 0 1.25rem;
  padding-bottom: 1.25rem;
  width: 100%;

  display: flex;
  align-items: center;
  justify-content: space-between;

  div {
    display: flex;

    &:first-child {
      margin-right: 0.5rem;
    }
  }

  span {
    font-size: 0.875rem;
    margin: 0;
    color: ${(props) => props.theme["base-text"]};
  }

  strong {
    margin: 0;
    font-size: 1.5rem;
    font-weight: 700;
    color: ${(props) => props.theme["base-text"]};
  }
`;
