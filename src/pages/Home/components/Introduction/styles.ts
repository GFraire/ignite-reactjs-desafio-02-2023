import styled, { css } from "styled-components";

interface IconContainerProps {
  variant: "yellow" | "yellow-dark" | "purple" | "base-text"
}

export const InfoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 5.75rem;
`;

export const TitleContainer = styled.div`
  max-width: 36.75rem;

  h1 {
    font-size: 3rem;
    font-family: "Baloo 2", sans-serif;
    font-weight: 800;
    line-height: 130%;
    color: ${props => props.theme["base-subtitle"]};
  }

  span {
    display: inline-block;
    margin-top: 1rem;
    color: ${(props) => props.theme["base-subtitle"]};
    font-size: 1.25rem;
    line-height: 130%;
  }
`;

export const ItemsContainer = styled.div`
  display: grid;
  grid-template-columns: .8fr 1fr;
  grid-template-rows: .8fr 1fr;

  margin-top: 4.125rem;

  div {
    display: flex;
    align-items: center;
    gap: 12px;

    font-size: 1rem;
    color: ${props => props.theme["base-text"]};

    &:nth-child(3), &:nth-child(4) {
      margin-top: 1.25rem;
    }
  }
`;

export const IconContainer = styled.div<IconContainerProps>`
  ${props => css`background-color: ${props.theme[props.variant]}`};
  
  display: flex;
  align-items: center;
  display: flex;
  flex-shrink: 1;

  border-radius: 999px;

  padding: .5rem;
`;