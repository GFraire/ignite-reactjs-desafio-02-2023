import { Coffee, Package, ShoppingCart, Timer } from "phosphor-react";
import {
  IconContainer,
  InfoContainer,
  ItemsContainer,
  TitleContainer,
} from "./styles";
import { useTheme } from "styled-components";
import coffeCup from "@/assets/coffe-cup.png";

export function Introduction() {
  const { white } = useTheme();

  return (
    <InfoContainer>
      <div>
        <TitleContainer>
          <h1>Encontre o café perfeito para qualquer hora do dia</h1>

          <span>
            Com o Coffee Delivery você recebe seu café onde estiver, a qualquer
            hora
          </span>
        </TitleContainer>

        <ItemsContainer>
          <div>
            <IconContainer variant="yellow-dark">
              <ShoppingCart weight="fill" color={white} size={16} />
            </IconContainer>

            <span>Compra simples e segura</span>
          </div>

          <div>
            <IconContainer variant="base-text">
              <Package weight="fill" color={white} size={16} />
            </IconContainer>

            <span>Embalagem mantém o café intacto</span>
          </div>

          <div>
            <IconContainer variant="yellow">
              <Timer weight="fill" color={white} size={16} />
            </IconContainer>

            <span>Entrega rápida e rastreada</span>
          </div>

          <div>
            <IconContainer variant="purple">
              <Coffee weight="fill" color={white} size={16} />
            </IconContainer>

            <span>O café chega fresquinho até você</span>
          </div>
        </ItemsContainer>
      </div>

      <img
        src={coffeCup}
        alt="Copo de café com a marca coffe delivery e com grãos de café ao fundo"
      />
    </InfoContainer>
  );
}
