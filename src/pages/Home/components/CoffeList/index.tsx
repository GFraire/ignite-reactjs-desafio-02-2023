import { CoffeItem } from "../CoffeItem";
import {
  ButtonFilterContainer,
  CallToActionContainer,
  CoffeItemsContainer,
  CoffeListContainer,
  TagsFilterContainer,
} from "./styles";
import coffeList from "./items.json";
import { useEffect, useState } from "react";

export function CoffeList() {
  const [tagsToFilter, setTagsToFilter] = useState<string[]>([]);
  const [filteredCoffeitems, setFilteredCoffeItems] = useState(coffeList);

  const tags = ["TRADICIONAL", "ESPECIAL", "COM LEITE", "ALCÓLICO", "GELADO"];

  useEffect(() => {
    if (tagsToFilter.length > 0) {
      const coffeListFiltered = coffeList.filter((coffeItem) => {
        const hasTagInCoffeItem = coffeItem.tags.some((tag) =>
          tagsToFilter.includes(tag)
        );

        if (hasTagInCoffeItem) return coffeItem;
      });

      setFilteredCoffeItems(coffeListFiltered);
    } else {
      setFilteredCoffeItems(coffeList);
    }
  }, [tagsToFilter]);

  function handleSetTagToFilter(tag: string) {
    if (tagsToFilter.includes(tag)) {
      let tagsWithoutSelectedTag = tagsToFilter.filter(
        (tagFilter) => tagFilter != tag
      );

      setTagsToFilter(tagsWithoutSelectedTag);
    } else {
      setTagsToFilter([...tagsToFilter, tag]);
    }
  }

  return (
    <CoffeListContainer>
      <CallToActionContainer>
        <h2>Nossos cafés</h2>

        <TagsFilterContainer>
          {tags.map((tag) => (
            <ButtonFilterContainer
              variant={tagsToFilter.includes(tag) ? "active" : undefined}
              key={tag}
              onClick={() => handleSetTagToFilter(tag)}
            >
              {tag}
            </ButtonFilterContainer>
          ))}
        </TagsFilterContainer>
      </CallToActionContainer>

      <CoffeItemsContainer>
        {filteredCoffeitems.map((coffeItem) => (
          <CoffeItem key={coffeItem.title} coffeItem={coffeItem} />
        ))}
      </CoffeItemsContainer>
    </CoffeListContainer>
  );
}
