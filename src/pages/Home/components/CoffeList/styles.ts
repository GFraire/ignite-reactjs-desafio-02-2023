import styled from "styled-components";

interface ButtonFilterContainerProps {
  variant: "active" | undefined
}

export const CoffeListContainer = styled.main`
  display: flex;
  flex-direction: column;
  margin-bottom: 2rem;

  h2 {
    font-family: "Baloo 2", sans-serif;
    font-size: 2rem;
    color: ${(props) => props.theme["base-subtitle"]};

    line-height: 130%;
    font-weight: 800;
  }
`;

export const CallToActionContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

export const TagsFilterContainer = styled.div`
  display: flex;
  gap: 8px;
`

export const ButtonFilterContainer = styled.button<ButtonFilterContainerProps>`
  cursor: pointer;
  border: 1px solid ${props => props.theme.yellow};
  background-color: ${props => props.variant === "active" ? props.theme["yellow-dark"] : "transparent"};
  border-radius: 100px;
  padding: 0px 12px;
  color: ${props => props.variant === "active" ? props.theme.white : props.theme["yellow-dark"]};
  font-size: .625rem;
  font-weight: bold;
  height: 25px;
`

export const CoffeItemsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 2rem;
`
