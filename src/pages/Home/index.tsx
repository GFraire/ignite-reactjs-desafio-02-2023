import { CoffeList } from "./components/CoffeList";
import { Introduction } from "./components/Introduction";
import {
  HomeContainer,
} from "./styles";

export function Home() {

  return (
    <HomeContainer>
      <Introduction />

      <CoffeList />
    </HomeContainer>
  );
}
