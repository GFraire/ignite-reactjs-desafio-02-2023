import styled from "styled-components";

export const HomeContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 70rem;
  margin: auto;
  margin-top: 5.875rem;
`;
