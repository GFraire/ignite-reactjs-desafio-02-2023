import { useContext } from "react";
import { SuccessContainer } from "./styles";
import { CartContext } from "@/contexts/CartContext";

export function Success() {
  const { address } = useContext(CartContext);

  return (
    <SuccessContainer>
      <div></div>
      <div>
        <span>
          Entrega em{" "}
          <strong>
            {address.rua}, {address.numero}
          </strong>{" "}
          {address.bairro}, {address.UF}
        </span>
      </div>
    </SuccessContainer>
  );
}
