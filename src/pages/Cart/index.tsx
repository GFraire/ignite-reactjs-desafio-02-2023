import { useContext } from "react";
import {
  CartContainer,
  CartItemsContainer,
  ConfirmationContainer,
  PricesContainer,
  SumaryCartContainer,
} from "./styles";
import { CartContext } from "@/contexts/CartContext";
import { CartItem } from "./CartItem";
import { formatToBRNumber } from "@/utils/formatting";
import { useNavigate } from "react-router-dom";
import { AddressForm } from "./AddressForm";

export function Cart() {
  const { totalOfItems, cartItems, isAddressValidated } = useContext(CartContext);
  const navigate = useNavigate();

  const shippingPrice = 3.5;

  const totalOfItemsFormatted = formatToBRNumber(totalOfItems);
  const shippingPriceFormatted = formatToBRNumber(shippingPrice);
  const totalFormatted = formatToBRNumber(totalOfItems + shippingPrice);

  function handleConfirmOrder() {
    navigate("/success");
  }

  return (
    <CartContainer>
      <div>
        <h4>Complete seu pedido</h4>

        <AddressForm />
      </div>

      <div>
        <h4>Cafés selecionados</h4>

        <SumaryCartContainer>
          {cartItems.length > 0 ? (
            <CartItemsContainer>
              {cartItems.map((cartItem) => (
                <CartItem key={cartItem.title} coffeItem={cartItem} />
              ))}
            </CartItemsContainer>
          ) : (
            <CartItemsContainer>
              <span>Nenhum item no carrinho</span>
            </CartItemsContainer>
          )}

          <PricesContainer>
            <div>
              <span>Total de itens</span>

              <span>R$ {totalOfItemsFormatted}</span>
            </div>

            <div>
              <span>Entrega</span>

              <span>R$ {shippingPriceFormatted}</span>
            </div>

            <div>
              <strong>Total</strong>

              <strong>R$ {totalFormatted}</strong>
            </div>
          </PricesContainer>

          <ConfirmationContainer
            disabled={cartItems.length === 0 || !isAddressValidated}
            onClick={handleConfirmOrder}
          >
            Confirmar pedido
          </ConfirmationContainer>
        </SumaryCartContainer>
      </div>
    </CartContainer>
  );
}
