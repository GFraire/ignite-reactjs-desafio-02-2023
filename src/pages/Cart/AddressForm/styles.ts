import styled from "styled-components";

export const AddressFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 2rem;
  padding: 2.5rem;
  
  background-color: ${props => props.theme["base-card"]};
  border-radius: 6px;
  margin-top: 1rem;

  & > div {
    display: flex;
    gap: 8px;
  }

  h4 {
    font-size: 1rem;
    color: ${props => props.theme["base-subtitle"]};
    font-family: "Roboto", "Arial";
    font-weight: 400;
  }

  span {
    color: ${props => props.theme["base-text"]};
    font-size: .875rem;
  }
`

export const FormContainer = styled.form`
  display: grid;
  grid-template-columns: 35% 50% 15%;
  gap: 16px;
`

export const AddressInput = styled.input`
  color: ${props => props.theme["base-label"]};
  background-color: ${props => props.theme["base-input"]};
  border: solid 1px ${props => props.theme["base-button"]};;
  border-radius: 4px;
  padding: 12px;
  outline: none;

  &:focus {
    outline: solid 1px ${props => props.theme["purple"]};
  }

  &[type=number]::-webkit-inner-spin-button,
  &[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  &[type=number] {
    -moz-appearance: textfield; /* Firefox */
  }

  &:nth-child(2) {
    grid-column-start: 1;
    grid-column-end: -1;
    width: calc(100% - 32px) !important;
  }

  &:nth-child(4) {
    grid-column-start: 2;
    grid-column-end: -1;
    width: calc(100% - 32px) !important;
  }

  &:nth-child(7) {
    width: calc(100% - 32px) !important;
  }`