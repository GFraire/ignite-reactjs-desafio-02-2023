import { useTheme } from "styled-components";
import { MapPinLine } from "phosphor-react";
import { AddressFormContainer, AddressInput, FormContainer } from "./styles";
import { ChangeEvent, useContext, useEffect, useState } from "react";
import { CartContext } from "@/contexts/CartContext";

export function AddressForm() {
  const { "yellow-dark": yellowDark } = useTheme();
  const { onSetAddress } = useContext(CartContext);
  const [CEP, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [numero, setNumero] = useState("");
  const [complemento, setComplemento] = useState("");
  const [bairro, setBairro] = useState("");
  const [cidade, setCidade] = useState("");
  const [UF, setUF] = useState("");

  useEffect(() => {
    const newAddress = {
      CEP,
      rua,
      numero,
      complemento,
      bairro,
      cidade,
      UF,
    };

    onSetAddress(newAddress);
  }, [CEP, rua, numero, cidade, bairro, cidade, UF]);

  function handleSetCEP(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;

    value = value.replace(/\D/g, "");

    // Adiciona o hífen, se necessário
    value = value.replace(/^(\d{5})(\d{3})/, "$1-$2");

    if (value.length <= 9) {
      setCEP(value);
    }
  }

  function handleSetRua(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;

    setRua(value);
  }

  function handleSetNumero(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;
    
    value = value.replace(/\D/g, "");

    setNumero(value);
  }

  function handleSetComplemento(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;

    setComplemento(value);
  }

  function handleSetBairro(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;

    setBairro(value);
  }

  function handleSetCidade(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;

    setCidade(value);
  }

  function handleSetUF(event: ChangeEvent<HTMLInputElement>) {
    let value = event.target.value;

    // Remover os números
    value = value.replace(/\d+/g, "");

    value = value.toUpperCase();

    if (value.length <= 2) setUF(value);
  }

  return (
    <AddressFormContainer>
      <div>
        <MapPinLine color={yellowDark} size={22} />

        <div>
          <h4>Endereço de entrega</h4>
          <span>Informe o endereço onde deseja receber seu pedido</span>
        </div>
      </div>

      <FormContainer>
        <AddressInput placeholder="CEP" value={CEP} onChange={handleSetCEP} />
        <AddressInput placeholder="Rua" value={rua} onChange={handleSetRua} />
        <AddressInput
          placeholder="Número"
          type="number"
          value={numero}
          onChange={handleSetNumero}
        />
        <AddressInput
          placeholder="Complemento"
          value={complemento}
          onChange={handleSetComplemento}
        />
        <AddressInput
          placeholder="Bairro"
          value={bairro}
          onChange={handleSetBairro}
        />
        <AddressInput
          placeholder="Cidade"
          value={cidade}
          onChange={handleSetCidade}
        />
        <AddressInput placeholder="UF" value={UF} onChange={handleSetUF} />
      </FormContainer>
    </AddressFormContainer>
  );
}
