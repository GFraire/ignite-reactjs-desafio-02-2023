import styled from "styled-components";

export const CartContainer = styled.div`
  display: flex;
  max-width: 70rem;
  margin: auto;
  margin-top: 5.875rem;
  gap: 32px;

  h4 {
    font-family: "Baloo 2", "Arial", "sans-serif";
    color: ${props => props.theme["base-subtitle"]};
    font-size: 1.125rem;
  }
  
  & > div:last-child {
    flex-shrink: 1;
  }
`;

export const SumaryCartContainer = styled.div`
  display: flex;
  flex-direction: column;

  padding: 2.5rem;
  margin-top: 1rem;
  background-color: ${props => props.theme["base-card"]};
  border-radius: 6px 44px 6px 44px;

  & > button {
    margin-top: 1.5rem;
  }
`

export const CartItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 206px;
  overflow-y: scroll;

  & > span {
    color: ${props => props.theme["base-text"]}
  }

  &::-webkit-scrollbar {
    width: 4px; /* Largura da barra de rolagem */
  }

  &::-webkit-scrollbar-track {
    border-radius: 10px; /* Raio das bordas da barra de rolagem */
    background-color: ${props => props.theme["base-button"]}; /* Cor de fundo da barra de rolagem */
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 10px; /* Raio das bordas do botão de rolagem */
    background-color: ${props => props.theme["base-label"]}; /* Cor do botão de rolagem */
  }

  &::-webkit-scrollbar-thumb:hover {
    background-color: ${props => props.theme["base-text"]};; /* Cor do botão de rolagem quando hover */
  }

  /* Esconda as setas da barra de rolagem */
  &::-webkit-scrollbar-button {
    display: none;
  }
`

export const PricesContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;
  width: 100%;
  margin-top: 2.5rem;

  div {
    display: flex;
    justify-content: space-between;
  }

  span {
    color: ${props => props.theme["base-text"]};

    &:first-child {
      font-size: .875rem;
    }

    &:last-child {
      font-size: 1rem;
    }
  }

  strong {
    color: ${props => props.theme["base-subtitle"]};
    font-size: 1.125rem;
  }
`

export const ConfirmationContainer = styled.button`
  background-color: ${props => props.theme["yellow"]};
  color: ${props => props.theme["white"]};
  text-transform: uppercase;
  padding: 12px 0;
  border: none;
  border-radius: 6px;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    filter: brightness(0.8);
  }
`