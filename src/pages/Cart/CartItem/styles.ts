import styled from "styled-components";

export const CartItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 8px 4px 24px 4px;
  border-bottom: 1px solid ${props => props.theme["base-button"]};

  & > span {
    display: flex;
    color: ${props => props.theme["base-text"]};
    font-size: 1rem;
    font-weight: bold;
    white-space: nowrap;
  }

  & + & {
    padding-top: 24px;
  }
`

export const DetailsContainer = styled.div`
  display: flex;
  gap: 20px;

  img {
    width: 64px;
  }
`

export const ActionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;

  span {
    color: ${props => props.theme["base-subtitle"]};
    font-size: 1rem;
  }

  div {
    display: flex;
    gap: 8px;
    width: 100%;
  }
`

