import { CoffeItemObject } from "@/pages/Home/components/CoffeItem"
import { ActionsContainer, CartItemContainer, DetailsContainer } from "./styles"
import { InputCart } from "@/components/InputCard"
import useImage from "@/hooks/useImage"

interface CartItemProps {
  coffeItem: CoffeItemObject
}

export function CartItem({ coffeItem }: CartItemProps) {
  const priceFormatted = coffeItem.price.toFixed(2).replace(".", ",")
  const imagePath = useImage(coffeItem.image)

  return (
    <CartItemContainer>
      <DetailsContainer>
        <img src={imagePath} alt={coffeItem.title} />

        <ActionsContainer>
          <span>{coffeItem.title}</span>

          <div>
            <InputCart coffeItem={coffeItem} />

            <InputCart coffeItem={coffeItem} variant="button" />
          </div>
        </ActionsContainer>
      </DetailsContainer>

      <span>R$ {priceFormatted}</span>
    </CartItemContainer>
  )
}